#-*- encoding:  utf-8 -*-

from django.db import models
from django.forms import ImageField

TYPE_OF =(
        ('C','CONCURSO'),
        ('V','VOTACIONES'),
    )
    
PRIZES = (
        ('$', 'Money'),
        ('iPhone', 'iPhone'),
        ('iPad','iPad'),
        ('Car','Car'),
        ('MB', 'Motorcycle'),
        ('OT', 'Other'),
    )
# Create your models here.
class Concurso(models.Model):
   
    typeOfGame = models.CharField("Tipo de Concurso", max_length=1, choices=TYPE_OF)
    showName = models.CharField("Nombre Programa", max_length=200)
    gameName = models.CharField("Nombre del Concurso", max_length=200)
    number905 = models.IntegerField("Número 905", default=0)
    numberSMS = models.IntegerField("Número SMS", default=0)
    gameDescription = models.CharField("Descripcion servicio", max_length=500)
    prize = models.CharField("Premio", max_length=10, choices=PRIZES)
    gameRules = models.CharField("Bases del Concurso", max_length=2000)
    gameImage = models.ImageField(upload_to='media/images/concursos/', default='media/images/concursos/logo-mediaset-es.png')

    
