"""from django.shortcuts import render
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from votaciones.models import Concurso
from votaciones.serializers import ConcursoSerializer

# Create your views here.
@api_view(['GET', 'POST'])
def concurso_list(request, format=None):
"""
#List all snippets, or create a new snippet.
"""
if request.method == 'GET':
concurso = Concurso.objects.all()
serializer = ConcursoSerializer(concurso, many=True)
return Response(serializer.data)

elif request.method == 'POST':
serializer = ConcursoSerializer(data=request.data)
if serializer.is_valid():
serializer.save()
return Response(serializer.data, status=status.HTTP_201_CREATED)
return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def concurso_detail(request, pk, format=None):
"""
#Retrieve, update or delete a snippet instance.
"""
try:
concurso = Concurso.objects.get(pk=pk)
except Concurso.DoesNotExist:
return Response(status=status.HTTP_404_NOT_FOUND)

if request.method == 'GET':
serializer = ConcursoSerializer(concurso)
return Response(serializer.data)

elif request.method == 'PUT':
serializer = ConcursoSerializer(concurso, data=request.data)
if serializer.is_valid():
serializer.save()
return Response(serializer.data)
return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

elif request.method == 'DELETE':
snippet.delete()
return Response(status=status.HTTP_204_NO_CONTENT)
"""

from votaciones.models import Concurso
from votaciones.serializers import ConcursoSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status


class ConcursoList(APIView):
    """
    List all concurso, or create a new snippet.
    """
    def get(self, request, format=None):
        concurso = Concurso.objects.all()
        serializer = ConcursoSerializer(concurso, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = ConcursoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ConcursoDetail(APIView):
    """
    Retrieve, update or delete a snippet instance.
    """
    def get_object(self, pk):
        try:
            return Concurso.objects.get(pk=pk)
        except Concurso.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        concurso = self.get_object(pk)
        serializer = ConcursoSerializer(concurso)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        concurso = self.get_object(pk)
        serializer = ConcursoSerializer(concurso, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
            concurso = self.get_object(pk)
            concurso.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)