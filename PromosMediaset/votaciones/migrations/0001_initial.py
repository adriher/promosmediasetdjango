# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Concurso',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('typeOfGame', models.CharField(max_length=1, verbose_name=b'Tipo de Concurso', choices=[(b'C', b'CONCURSO'), (b'V', b'VOTACIONES')])),
                ('showName', models.CharField(max_length=200, verbose_name=b'Nombre Programa')),
                ('gameName', models.CharField(max_length=200, verbose_name=b'Nombre del Concurso')),
                ('number905', models.IntegerField(default=0, verbose_name=b'N\xc3\xbamero 905')),
                ('numberSMS', models.IntegerField(default=0, verbose_name=b'N\xc3\xbamero SMS')),
                ('gameDescription', models.CharField(max_length=500, verbose_name=b'Descripcion servicio')),
                ('prize', models.CharField(max_length=10, verbose_name=b'Premio', choices=[(b'$', b'Money'), (b'iPhone', b'iPhone'), (b'iPad', b'iPad'), (b'Car', b'Car'), (b'MB', b'Motorcycle'), (b'OT', b'Other')])),
                ('gameRules', models.CharField(max_length=2000, verbose_name=b'Bases del Concurso')),
                ('gameImage', models.ImageField(default=b'concursos/logo-mediaset-es.png', upload_to=b'media/images/concursos/')),
            ],
        ),
    ]
