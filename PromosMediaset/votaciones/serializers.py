#-*- encoding:  utf-8 -*-

from django.forms import widgets
from rest_framework import serializers
from votaciones.models import Concurso, TYPE_OF, PRIZES

class ConcursoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Concurso
        fields = ('typeOfGame','showName',  'gameName',  'number905',  'numberSMS',   'gameDescription', 'prize',  'gameRules', 'gameImage')